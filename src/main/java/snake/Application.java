package snake;

import snake.ai.ShortestDistanceAI;
import snake.ui.gui.window.GameWindow;
import snake.ui.gui.window.MenuWindow;
import snake.util.Updater;

import java.io.IOException;

public class Application {

    private static final class CLI implements Updater {
        private final Snake snake;

        private CLI(Snake snake) {
            this.snake = snake;
            this.snake.setUpdater(this);
        }

        @Override
        public void update() {
            Application.drawCLISnake(this.snake);
        }
    }

    public static void main(String[] args) {
        var ui = new MenuWindow();
        ui.run();
    }

    private static void drawCLISnake(Snake snake) {
        var snakeBody = snake.getSnakeSegments();
        for (int y = 0; y < snake.getPlaneSize(); y += 1) {
            for (int x = 0; x < snake.getPlaneSize(); x += 1) {
                var currentPosition = new Snake.Position(x, y, snake.getPlaneSize());
                if (snakeBody.contains(currentPosition)) {
                    System.out.print('#');
                } else if (currentPosition.equals(snake.getApplePosition())) {
                    System.out.print('A');
                } else {
                    System.out.print('.');
                }
            }
            System.out.print('\n');
        }
    }
}
