package snake.util;

import snake.util.settings.DIFFICULTY;
import snake.util.settings.PLANE_SIZE;
import snake.util.settings.UI_PANEL_SIZE;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public final class PropertiesManager {
    private static final String PROPERTIES_FILE_NAME = "/config/settings.properties";
    private static final String DEFAULT_PROPERTIES_STRING =
            DIFFICULTY.PROPERTY_HARD_RATE + "=20\n" +
                    DIFFICULTY.PROPERTY_MEDIUM_RATE + "=15\n" +
                    DIFFICULTY.PROPERTY_EASY_RATE + "=10\n" +
                    DIFFICULTY.PROPERTY_AUTO1_RATE + "=10\n" +
                    DIFFICULTY.PROPERTY_AUTO2_RATE + "=50\n" +
                    DIFFICULTY.PROPERTY_AUTO3_RATE + "=100\n" +

                    PLANE_SIZE.PROPERTY_SMALL_SIZE + "=10\n" +
                    PLANE_SIZE.PROPERTY_MEDIUM_SIZE + "=20\n" +
                    PLANE_SIZE.PROPERTY_LARGE_SIZE + "=30\n" +

                    UI_PANEL_SIZE.PROPERTY_ICON_SIZE + "=16\n" +

                    UI_PANEL_SIZE.PROPERTY_SMALL_SIZE + "=160\n" +
                    UI_PANEL_SIZE.PROPERTY_MEDIUM_SIZE + "=320\n" +
                    UI_PANEL_SIZE.PROPERTY_LARGE_SIZE + "=480\n" +
                    UI_PANEL_SIZE.PROPERTY_VERY_LARGE_SIZE + "=640";

    private static final Properties properties;
    private static final Properties defaultProperties;

    static {
        properties = new Properties();
        defaultProperties = new Properties();

        loadProperties();
        loadDefaults();
    }

    private static void loadDefaults() {
        try {
            defaultProperties.load(new StringReader(DEFAULT_PROPERTIES_STRING));
        } catch (IOException ignored) { // Should never happen
        }
    }

    private static void loadProperties() {
        var is = PropertiesManager.class.getResourceAsStream(PROPERTIES_FILE_NAME);
        if (is == null) {
            return;
        }
        try {
            properties.load(is);
        } catch (IOException ignored) {
            // Default properties will be used instead.
            // User doesn't need to know something went wrong.
        }
    }

    public static String getValue(String propertyName) {
        return properties.getProperty(propertyName, defaultProperties.getProperty(propertyName));
    }
}
