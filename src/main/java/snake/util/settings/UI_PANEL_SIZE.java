package snake.util.settings;

import snake.util.PropertiesManager;

import java.util.logging.Level;
import java.util.logging.Logger;

public enum UI_PANEL_SIZE {
    SMALL,
    MEDIUM,
    LARGE,
    VERY_LARGE;

    public static final String PROPERTY_SMALL_SIZE = "UI.Panel.Small.Size";
    public static final String PROPERTY_MEDIUM_SIZE = "UI.Panel.Medium.Size";
    public static final String PROPERTY_LARGE_SIZE = "UI.Panel.Large.Size";
    public static final String PROPERTY_VERY_LARGE_SIZE = "UI.Panel.VeryLarge.Size";

    public static final String PROPERTY_ICON_SIZE = "Icon.Size";

    private static final Logger logger = Logger.getLogger(UI_PANEL_SIZE.class.getSimpleName());

    @Override
    public String toString() {
        switch (this) {
            case SMALL:
                return "Small";
            case MEDIUM:
                return "Medium";
            case LARGE:
                return "Large";
            case VERY_LARGE:
                return "Very Large";
            default:
                throw new IllegalStateException("unexpected value");
        }
    }

    public static UI_PANEL_SIZE fromString(String repr) {
        switch (repr) {
            case "Small":
                return SMALL;
            case "Medium":
                return MEDIUM;
            case "Large":
                return LARGE;
            case "Very Large":
                return VERY_LARGE;
            default:
                throw new IllegalStateException("unexpected value: " + repr);
        }
    }

    public int getSize() {
        final int DEFAULT_SIZE = 10;
        try {
            switch (this) {
                case SMALL:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_SMALL_SIZE));
                case MEDIUM:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_MEDIUM_SIZE));
                case LARGE:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_LARGE_SIZE));
                case VERY_LARGE:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_VERY_LARGE_SIZE));
                default:
                    throw new IllegalStateException("Unexpected value: " + this);
            }
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING, "cannot parse size", e);
            return DEFAULT_SIZE;
        }
    }

    public static int getIconSize() {
        final int DEFAULT_SIZE = 16;
        try {
            return Integer.parseInt(PropertiesManager.getValue(PROPERTY_ICON_SIZE));
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING, "cannot parse size", e);
            return DEFAULT_SIZE;
        }
    }
}
