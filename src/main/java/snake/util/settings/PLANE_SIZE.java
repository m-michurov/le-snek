package snake.util.settings;

import snake.util.PropertiesManager;

import java.util.logging.Level;
import java.util.logging.Logger;

public enum PLANE_SIZE {
    SMALL,
    MEDIUM,
    LARGE;

    public static final String PROPERTY_SMALL_SIZE = "Plane.Small.Size";
    public static final String PROPERTY_MEDIUM_SIZE = "Plane.Medium.Size";
    public static final String PROPERTY_LARGE_SIZE = "Plane.Large.Size";

    private static final Logger logger = Logger.getLogger(PLANE_SIZE.class.getSimpleName());

    @Override
    public String toString() {
        switch (this) {
            case SMALL:
                return "Small";
            case MEDIUM:
                return "Medium";
            case LARGE:
                return "Large";
            default:
                throw new IllegalStateException("unexpected value");
        }
    }

    public static PLANE_SIZE fromString(String repr) {
        switch (repr) {
            case "Small":
                return SMALL;
            case "Medium":
                return MEDIUM;
            case "Large":
                return LARGE;
            default:
                throw new IllegalStateException("unexpected value: " + repr);
        }
    }

    public int getSize() {
        final int DEFAULT_SIZE = 10;
        try {
            switch (this) {
                case SMALL:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_SMALL_SIZE));
                case MEDIUM:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_MEDIUM_SIZE));
                case LARGE:
                    return Integer.parseInt(PropertiesManager.getValue(PROPERTY_LARGE_SIZE));
                default:
                    throw new IllegalStateException("Unexpected value: " + this);
            }
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING, "cannot parse size", e);
            return DEFAULT_SIZE;
        }
    }
}
