package snake.util.settings;

import snake.ai.AI;
import snake.ai.BFSShortestDistanceAI;
import snake.ai.HamiltonianCycleAI;
import snake.ai.ShortestDistanceAI;
import snake.util.PropertiesManager;

import java.util.logging.Level;
import java.util.logging.Logger;

public enum DIFFICULTY {
    HARD,
    MEDIUM,
    EASY,
    AUTO1,
    AUTO2,
    AUTO3;

    public static final int TOTAL_LEVELS = 6;

    public static final String PROPERTY_HARD_RATE = "Difficulty.Hard.Rate";
    public static final String PROPERTY_MEDIUM_RATE = "Difficulty.Medium.Rate";
    public static final String PROPERTY_EASY_RATE = "Difficulty.Easy.Rate";
    public static final String PROPERTY_AUTO1_RATE = "Difficulty.Auto1.Rate";
    public static final String PROPERTY_AUTO2_RATE = "Difficulty.Auto2.Rate";
    public static final String PROPERTY_AUTO3_RATE = "Difficulty.Auto3.Rate";

    private static final Logger logger = Logger.getLogger(DIFFICULTY.class.getSimpleName());

    @Override
    public String toString() {
        switch (this) {
            case HARD:
                return "Hard";
            case MEDIUM:
                return "Medium";
            case EASY:
                return "Easy";
            case AUTO1:
                return "Auto #1";
            case AUTO2:
                return "Auto #2";
            case AUTO3:
                return "Auto #3";
            default:
                throw new IllegalStateException("unexpected value");
        }
    }

    public static DIFFICULTY fromString(String repr) {
        switch (repr) {
            case "Hard":
                return HARD;
            case "Medium":
                return MEDIUM;
            case "Easy":
                return EASY;
            case "Auto #1":
                return AUTO1;
            case "Auto #2":
                return AUTO2;
            case "Auto #3":
                return AUTO3;
            default:
                throw new IllegalStateException("unexpected value: " + repr);
        }
    }

    public double getRate() {
        final double DEFAULT_RATE = 10;
        try {
            switch (this) {
                case HARD:
                    return Double.parseDouble(PropertiesManager.getValue(PROPERTY_HARD_RATE));
                case MEDIUM:
                    return Double.parseDouble(PropertiesManager.getValue(PROPERTY_MEDIUM_RATE));
                case EASY:
                    return Double.parseDouble(PropertiesManager.getValue(PROPERTY_EASY_RATE));
                case AUTO1:
                    return Double.parseDouble(PropertiesManager.getValue(PROPERTY_AUTO1_RATE));
                case AUTO2:
                    return Double.parseDouble(PropertiesManager.getValue(PROPERTY_AUTO2_RATE));
                case AUTO3:
                    return Double.parseDouble(PropertiesManager.getValue(PROPERTY_AUTO3_RATE));
                default:
                    throw new IllegalStateException("Unexpected value: " + this);
            }
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING, "cannot parse rate", e);
            return DEFAULT_RATE;
        }
    }

    public boolean isNotAuto() {
        switch (this) {
            case AUTO1:
            case AUTO2:
            case AUTO3:
                return false;
            default:
                return true;
        }
    }

    public AI getCorrespondingAI() {
        if (this.isNotAuto()) {
            return null;
        }

        switch (this) {
            case AUTO1:
                return new ShortestDistanceAI();
            case AUTO2:
                return new BFSShortestDistanceAI();
            default:
            case AUTO3:
                return new HamiltonianCycleAI();
        }
    }
}
