package snake.util;

public interface Updater {

    static void updateIfNotNull(Updater updater) {
        if (updater != null) {
            updater.update();
        }
    }

    void update();
}
