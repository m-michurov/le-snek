package snake.ai;

import snake.Snake;
import snake.ai.util.Algorithms;

public class BFSShortestDistanceAI implements AI {

    @Override
    public Snake.DIRECTION getNextDirection(Snake snake) {
        return Algorithms.FindDirectionBFS(snake);
    }
}
