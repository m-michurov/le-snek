package snake.ai;

import snake.Snake;

public interface AI {

    Snake.DIRECTION getNextDirection(Snake snake);
}
