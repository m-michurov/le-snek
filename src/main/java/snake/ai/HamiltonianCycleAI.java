package snake.ai;

import snake.Snake;

public class HamiltonianCycleAI implements AI {

    @Override
    public Snake.DIRECTION getNextDirection(Snake snake) {
        var head = snake.getHeadPosition();

        if (head.X % 2 == 0) {
            if (head.Y == 0) {
                return Snake.DIRECTION.RIGHT;
            } else if (head.Y == snake.getPlaneSize() - 1) {
                if (head.X == 0) {
                    return Snake.DIRECTION.UP;
                } else {
                    return Snake.DIRECTION.LEFT;
                }
            } else if (head.Y == snake.getPlaneSize() - 2) {
                return Snake.DIRECTION.UP;
            } else {
                return Snake.DIRECTION.UP;
            }
        } else {
            if (head.Y == 0) {
                return Snake.DIRECTION.DOWN;
            } else if (head.Y == snake.getPlaneSize() - 1) {
                return Snake.DIRECTION.LEFT;
            } else if (head.Y == snake.getPlaneSize() - 2) {
                if (head.X == snake.getPlaneSize() - 1) {
                    return Snake.DIRECTION.DOWN;
                }
                return Snake.DIRECTION.RIGHT;
            } else {
                return Snake.DIRECTION.DOWN;
            }
        }
    }
}
