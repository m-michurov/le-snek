package snake.ai;

import snake.Snake;

public class ShortestDistanceAI implements AI {

    @Override
    public Snake.DIRECTION getNextDirection(Snake snake) {
        var head = snake.getHeadPosition();
        var apple = snake.getApplePosition();

        Snake.DIRECTION direction = Snake.DIRECTION.UP;
        var bestPosition = new Snake.Position(head.X, head.Y, snake.getPlaneSize());
        Snake.Position currentPosition;

        currentPosition = new Snake.Position(head.X - 1, head.Y, snake.getPlaneSize());
        if (currentPosition.distanceTo(apple) < bestPosition.distanceTo(apple)) {
            bestPosition = currentPosition;
            direction = Snake.DIRECTION.LEFT;
        }

        currentPosition = new Snake.Position(head.X + 1, head.Y, snake.getPlaneSize());
        if (currentPosition.distanceTo(apple) < bestPosition.distanceTo(apple)) {
            bestPosition = currentPosition;
            direction = Snake.DIRECTION.RIGHT;
        }

        currentPosition = new Snake.Position(head.X, head.Y - 1, snake.getPlaneSize());
        if (currentPosition.distanceTo(apple) < bestPosition.distanceTo(apple)) {
            bestPosition = currentPosition;
            direction = Snake.DIRECTION.UP;
        }

        currentPosition = new Snake.Position(head.X, head.Y + 1, snake.getPlaneSize());
        if (currentPosition.distanceTo(apple) < bestPosition.distanceTo(apple)) {
            direction = Snake.DIRECTION.DOWN;
        }

        return direction;
    }
}
