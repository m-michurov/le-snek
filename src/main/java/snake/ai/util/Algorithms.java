package snake.ai.util;

import snake.Snake;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;

public class Algorithms {

    public static Snake.DIRECTION FindDirectionBFS(Snake snake) {
        var start = snake.getHeadPosition();
        var end = snake.getApplePosition();

        var queue = new ArrayDeque<Snake.Position>(snake.getPlaneSize() * snake.getPlaneSize());
        queue.add(start);

        var parent = new HashMap<Snake.Position, Snake.Position>();
        parent.put(start, start);

        var visited = new HashSet<Snake.Position>();
        visited.add(start);

        boolean pathExists = false;

        while (!queue.isEmpty()) {
            var current = queue.poll();

            if (current.equals(end)) {
                pathExists = true;
                break;
            }

            for (var adjacent : current.getNeighbours()) {
                if (snake.getSnakeSegments().contains(adjacent)) {
                    continue;
                }

                if (!visited.contains(adjacent)) {
                    visited.add(adjacent);
                    parent.put(adjacent, current);
                    queue.add(adjacent);
                }
            }
        }

        if (!pathExists) {
            for (var alternative : start.getNeighbours()) {
                if (!snake.getSnakeSegments().contains(alternative)) {
                    return start.getMinDistanceDirection(alternative);
                }
            }
            return Snake.DIRECTION.UP;
        }

        Snake.Position current = end;
        while (!parent.get(current).equals(start)) {
            current = parent.get(current);
        }

        return start.getMinDistanceDirection(current);
    }
}
