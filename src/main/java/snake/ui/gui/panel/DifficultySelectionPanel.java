package snake.ui.gui.panel;

import snake.ui.gui.util.Utility;
import snake.ui.gui.window.MenuWindow;
import snake.util.settings.DIFFICULTY;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public final class DifficultySelectionPanel extends JPanel {
    private final MenuWindow mainMenuWindow;

    private DIFFICULTY selectedDifficulty;
    private JLabel selectedJLabel;

    public DifficultySelectionPanel(MenuWindow mainMenuWindow) {
        this.mainMenuWindow = mainMenuWindow;
        this.init();
    }

    private JLabel createLabel(
            DIFFICULTY difficulty,
            boolean selected) {
        final int WIDTH = 80;
        final var label = Utility.createLabel(difficulty.toString(), selected, SwingConstants.CENTER);
        label.setSize(WIDTH, label.getHeight());

        if (selected) {
            this.selectedJLabel = label;
            this.selectedDifficulty = difficulty;
        }

        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                DifficultySelectionPanel.this.selectedJLabel.setBorder(BorderFactory.createEmptyBorder());
                label.setBorder(BorderFactory.createLineBorder(Color.WHITE));
                DifficultySelectionPanel.this.selectedJLabel = label;
                DifficultySelectionPanel.this.selectedDifficulty = DIFFICULTY.fromString(
                        DifficultySelectionPanel.this.selectedJLabel.getText()
                );
            }
        });

        return label;
    }

    private void init() {
        this.setLayout(new BorderLayout());
        this.setBackground(Color.BLACK);

        var title = Utility.createLabel("Select Difficulty", false, SwingConstants.CENTER);
        this.add(title, BorderLayout.NORTH);

        var difficultyList = new JPanel(new GridLayout(DIFFICULTY.TOTAL_LEVELS / 2, 2));
        difficultyList.setBackground(Color.BLACK);

        difficultyList.add(this.createLabel(DIFFICULTY.HARD, false));
        difficultyList.add(this.createLabel(DIFFICULTY.AUTO1, false));
        difficultyList.add(this.createLabel(DIFFICULTY.MEDIUM, true));
        difficultyList.add(this.createLabel(DIFFICULTY.AUTO2, false));
        difficultyList.add(this.createLabel(DIFFICULTY.EASY, false));
        difficultyList.add(this.createLabel(DIFFICULTY.AUTO3, false));

        this.add(difficultyList, BorderLayout.CENTER);

        var controlPanel = new JPanel(new GridLayout(1, 2));

        var backButton = new JButton("Back");
        backButton.setBackground(Color.BLACK);
        backButton.setForeground(Color.WHITE);

        backButton.addActionListener((actionEvent) -> this.mainMenuWindow.showPanel(MenuWindow.MAIN_MENU_CARD));

        controlPanel.add(backButton);

        var startButton = new JButton("Start");
        startButton.setBackground(Color.BLACK);
        startButton.setForeground(Color.WHITE);

        startButton.addActionListener((actionEvent) -> this.mainMenuWindow.startGame());

        controlPanel.add(startButton);

        this.add(controlPanel, BorderLayout.SOUTH);
    }

    public DIFFICULTY getSelectedDifficulty() {
        return this.selectedDifficulty;
    }
}
