package snake.ui.gui.panel;

import snake.ui.gui.window.MenuWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class MainMenuPanel extends JPanel {
    private final MenuWindow mainMenuWindow;

    public MainMenuPanel(MenuWindow mainMenuWindow) {
        this.mainMenuWindow = mainMenuWindow;

        this.init();
    }

    private void init() {
        this.setLayout(new GridLayout(3, 1));
        this.setBackground(Color.BLACK);

        var startButton = new JButton("New Game");
        startButton.setBackground(Color.BLACK);
        startButton.setForeground(Color.WHITE);

        startButton.addActionListener((actionEvent) -> this.mainMenuWindow.showPanel(MenuWindow.DIFFICULTY_MENU_CARD));

        var settingsButton = new JButton("Settings");
        settingsButton.setBackground(Color.BLACK);
        settingsButton.setForeground(Color.WHITE);

        settingsButton.addActionListener((actionEvent) -> this.mainMenuWindow.showPanel(MenuWindow.SETTINGS_CARD));

        var exitButton = new JButton("Exit");
        exitButton.setBackground(Color.BLACK);
        exitButton.setForeground(Color.WHITE);

        exitButton.addActionListener((actionEvent) -> this.mainMenuWindow.dispatchEvent(new WindowEvent(this.mainMenuWindow, WindowEvent.WINDOW_CLOSING)));

        this.add(startButton);
        this.add(settingsButton);
        this.add(exitButton);
    }
}
