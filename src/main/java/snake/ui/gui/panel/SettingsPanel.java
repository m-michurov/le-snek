package snake.ui.gui.panel;

import snake.ui.gui.util.Utility;
import snake.ui.gui.window.MenuWindow;
import snake.util.settings.PLANE_SIZE;
import snake.util.settings.UI_PANEL_SIZE;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public final class SettingsPanel extends JPanel {
    private static final int SUBPANEL_TITLE_WIDTH = 70;

    private final MenuWindow mainMenuWindow;

    private PlaneSizeSelectionPanel planeSizeSelectionPanel;
    private UIPanelSizeSelectionPanel uiPanelSizeSelectionPanel;

    public SettingsPanel(MenuWindow mainMenuWindow) {
        this.mainMenuWindow = mainMenuWindow;
        this.init();
    }

    private void init() {
        this.setLayout(new BorderLayout());
        this.setBackground(Color.BLACK);

        var title = Utility.createLabel("Settings", false, SwingConstants.CENTER);
        this.add(title, BorderLayout.NORTH);
        this.add(new JSeparator(), BorderLayout.CENTER);

        var mainPanel = new JPanel(new GridLayout(2, 1));

        this.planeSizeSelectionPanel = new PlaneSizeSelectionPanel();
        mainPanel.add(this.planeSizeSelectionPanel);

        this.uiPanelSizeSelectionPanel = new UIPanelSizeSelectionPanel();
        mainPanel.add(this.uiPanelSizeSelectionPanel);

        this.add(mainPanel, BorderLayout.CENTER);

        var backButton = new JButton("Back");
        backButton.setBackground(Color.BLACK);
        backButton.setForeground(Color.WHITE);

        backButton.addActionListener((actionEvent) -> this.mainMenuWindow.showPanel(MenuWindow.MAIN_MENU_CARD));

        this.add(backButton, BorderLayout.SOUTH);
    }

    private static final class PlaneSizeSelectionPanel extends JPanel {
        private JLabel selectedPlaneSizeLabel;
        private PLANE_SIZE selectedPlaneSize;

        private PlaneSizeSelectionPanel() {
            this.init();
        }

        private JLabel createLabel(
                PLANE_SIZE planeSize,
                boolean selected) {
            final int WIDTH = 100;
            final var label = Utility.createLabel(planeSize.toString(), selected, SwingConstants.CENTER);
            label.setPreferredSize(new Dimension(WIDTH, label.getPreferredSize().height));

            if (selected) {
                this.selectedPlaneSizeLabel = label;
                this.selectedPlaneSize = planeSize;
            }

            label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    PlaneSizeSelectionPanel.this.selectedPlaneSizeLabel.setBorder(BorderFactory.createEmptyBorder());
                    label.setBorder(BorderFactory.createLineBorder(Color.WHITE));
                    PlaneSizeSelectionPanel.this.selectedPlaneSizeLabel = label;
                    PlaneSizeSelectionPanel.this.selectedPlaneSize = PLANE_SIZE.fromString(
                            PlaneSizeSelectionPanel.this.selectedPlaneSizeLabel.getText()
                    );
                }
            });

            return label;
        }

        private void init() {
            this.setLayout(new BorderLayout());
            this.setBackground(Color.BLACK);

            var title = Utility.createLabel("Plane Size", false, SwingConstants.CENTER);
            title.setPreferredSize(new Dimension(SUBPANEL_TITLE_WIDTH, title.getHeight()));
            this.add(title, BorderLayout.WEST);

            var optionsPlane = new JPanel(new GridLayout(1, 3));
            optionsPlane.setBackground(Color.BLACK);
            var smallSizeLabel = this.createLabel(PLANE_SIZE.SMALL, true);
            var mediumSizeLabel = this.createLabel(PLANE_SIZE.MEDIUM, false);
            var largeSizeLabel = this.createLabel(PLANE_SIZE.LARGE, false);

            optionsPlane.add(smallSizeLabel);
            optionsPlane.add(mediumSizeLabel);
            optionsPlane.add(largeSizeLabel);

            this.add(optionsPlane, BorderLayout.CENTER);
        }

        public PLANE_SIZE getSelectedPlaneSize() {
            return this.selectedPlaneSize;
        }
    }

    private static final class UIPanelSizeSelectionPanel extends JPanel {
        private JLabel selectedUIPanelSizeLabel;
        private UI_PANEL_SIZE selectedUIPanelSize;

        private UIPanelSizeSelectionPanel() {
            this.init();
        }

        private JLabel createLabel(
                UI_PANEL_SIZE panelSize,
                boolean selected) {
            final int WIDTH = 75;
            final var label = Utility.createLabel(panelSize.toString(), selected, SwingConstants.CENTER);
            label.setPreferredSize(new Dimension(WIDTH, label.getPreferredSize().height));

            if (selected) {
                this.selectedUIPanelSizeLabel = label;
                this.selectedUIPanelSize = panelSize;
            }

            label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    UIPanelSizeSelectionPanel.this.selectedUIPanelSizeLabel.setBorder(BorderFactory.createEmptyBorder());
                    label.setBorder(BorderFactory.createLineBorder(Color.WHITE));
                    UIPanelSizeSelectionPanel.this.selectedUIPanelSizeLabel = label;
                    UIPanelSizeSelectionPanel.this.selectedUIPanelSize = UI_PANEL_SIZE.fromString(
                            UIPanelSizeSelectionPanel.this.selectedUIPanelSizeLabel.getText()
                    );
                }
            });

            return label;
        }

        private void init() {
            this.setLayout(new BorderLayout());
            this.setBackground(Color.BLACK);

            var title = Utility.createLabel("UI Size", false, SwingConstants.CENTER);
            title.setPreferredSize(new Dimension(SUBPANEL_TITLE_WIDTH, title.getHeight()));
            this.add(title, BorderLayout.WEST);

            var optionsPlane = new JPanel(new GridLayout(1, 3));
            optionsPlane.setBackground(Color.BLACK);
            var smallSizeLabel = this.createLabel(UI_PANEL_SIZE.SMALL, true);
            var mediumSizeLabel = this.createLabel(UI_PANEL_SIZE.MEDIUM, false);
            var largeSizeLabel = this.createLabel(UI_PANEL_SIZE.LARGE, false);
            var veryLargeSizeLabel = this.createLabel(UI_PANEL_SIZE.VERY_LARGE, false);

            optionsPlane.add(smallSizeLabel);
            optionsPlane.add(mediumSizeLabel);
            optionsPlane.add(largeSizeLabel);
            optionsPlane.add(veryLargeSizeLabel);

            this.add(optionsPlane, BorderLayout.CENTER);
        }

        public UI_PANEL_SIZE getSelectedUIPanelSize() {
            return this.selectedUIPanelSize;
        }
    }

    public PLANE_SIZE getSelectedPlaneSize() {
        return this.planeSizeSelectionPanel.getSelectedPlaneSize();
    }

    public UI_PANEL_SIZE getSelectedUIPanelSize() {
        return this.uiPanelSizeSelectionPanel.getSelectedUIPanelSize();
    }
}
