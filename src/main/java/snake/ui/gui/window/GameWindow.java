package snake.ui.gui.window;

import snake.Snake;
import snake.ai.AI;
import snake.util.Updater;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class GameWindow extends JFrame implements Updater, Runnable {
    private final Snake snake;
    private final int planeSize;
    private final JLabel[] labelsGrid;
    private final IconManager iconManager;
    private final ScorePanel scorePanel;

    private final MenuWindow menuWindow;
    private final boolean useUserInput;
    private final AI snakeAI;

    private static final class IconManager {
        private static final int EMPTY = 0;
        private static final int SNAKE = 1;
        private static final int APPLE = 2;

        private static final int TOTAL_ICONS = 3;

        private final ImageIcon[] icons;

        private IconManager()
                throws IOException {
            this.icons = new ImageIcon[TOTAL_ICONS];

            this.loadIcons(1.0);
        }

        private IconManager(double scale)
                throws IOException {
            this.icons = new ImageIcon[TOTAL_ICONS];

            this.loadIcons(scale);
        }

        private void loadIcons(double scale)
                throws IOException {
            for (int i = 0; i < TOTAL_ICONS; i++) {

                var path = String.format("/icons/%d.png", i);
                var data = this.getClass().getResource(path).openStream();
                var bytes = data.readAllBytes();

                var icon = new ImageIcon(bytes);

                this.icons[i] = new ImageIcon(icon.getImage().getScaledInstance(
                        (int) (icon.getIconWidth() * scale),
                        (int) (icon.getIconHeight() * scale),
                        Image.SCALE_SMOOTH
                ));
            }
        }

        public ImageIcon getIcon(int iconIndex) {
            if (iconIndex < 0 || iconIndex >= TOTAL_ICONS) {
                throw new IllegalArgumentException(
                        String.format("icon index %d out of range [%d, %d]", iconIndex, 0, TOTAL_ICONS - 1));
            }
            return this.icons[iconIndex];
        }
    }

    private final class PlayerInputListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            var key = e.getKeyCode();

            switch (key) {
                case KeyEvent.VK_UP:
                    GameWindow.this.snake.setDirection(Snake.DIRECTION.UP);
                    break;
                case KeyEvent.VK_DOWN:
                    GameWindow.this.snake.setDirection(Snake.DIRECTION.DOWN);
                    break;
                case KeyEvent.VK_LEFT:
                    GameWindow.this.snake.setDirection(Snake.DIRECTION.LEFT);
                    break;
                case KeyEvent.VK_RIGHT:
                    GameWindow.this.snake.setDirection(Snake.DIRECTION.RIGHT);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    private static final class ScorePanel extends JPanel {
        private final JLabel scoreLabel;

        private ScorePanel() {
            this.setBackground(Color.BLACK);
            this.setLayout(new BorderLayout());

            this.scoreLabel = new JLabel("0");
            this.scoreLabel.setForeground(Color.WHITE);

            this.add(this.scoreLabel, BorderLayout.EAST);

            var title = new JLabel("Score");
            title.setForeground(Color.WHITE);
            this.add(title, BorderLayout.WEST);
        }

        private void setScore(int score) {
            this.scoreLabel.setText(String.valueOf(score));
        }
    }

    public GameWindow(
            MenuWindow menuWindow,
            Snake snake,
            double scale,
            boolean useUserInput,
            AI ai)
            throws IOException {
        super("Metal Gear Solid 3: Snake Eater");

        if (!useUserInput && ai == null) {
            throw new IllegalArgumentException("ai must be non-null if useUserInput is set to false");
        }

        this.menuWindow = menuWindow;

        this.useUserInput = useUserInput;
        this.snakeAI = ai;

        this.snake = snake;
        this.snake.setUpdater(this);
        this.planeSize = this.snake.getPlaneSize();

        this.labelsGrid = new JLabel[this.planeSize * this.planeSize];
        this.iconManager = new IconManager(scale);
        this.scorePanel = new ScorePanel();

        this.addComponentsToPane(this.getContentPane());
        this.addKeyListener(new PlayerInputListener());
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                GameWindow.this.snake.stop();
                GameWindow.this.showMenu();
            }
        });

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    private void initGrid() {
        for (int y = 0; y < this.planeSize; y += 1) {
            for (int x = 0; x < this.planeSize; x += 1) {
                this.labelsGrid[y * this.planeSize + x] = new JLabel();

                var currentPosition = new Snake.Position(x, y, this.planeSize);
                if (this.snake.getSnakeSegments().contains(currentPosition)) {
                    this.labelsGrid[y * this.planeSize + x].setIcon(this.iconManager.getIcon(IconManager.SNAKE));
                } else if (currentPosition.equals(this.snake.getApplePosition())) {
                    this.labelsGrid[y * this.planeSize + x].setIcon(this.iconManager.getIcon(IconManager.APPLE));
                } else {
                    this.labelsGrid[y * this.planeSize + x].setIcon(this.iconManager.getIcon(IconManager.EMPTY));
                }
            }
        }
    }

    private void addComponentsToPane(Container pane) {
        var mainPanel = new JPanel(new GridLayout(this.planeSize, this.planeSize));

        this.initGrid();

        for (int offset = 0; offset < this.planeSize * this.planeSize; offset += 1) {
            mainPanel.add(this.labelsGrid[offset]);
        }

        pane.add(mainPanel, BorderLayout.NORTH);
        pane.add(new JSeparator(), BorderLayout.CENTER);
        pane.add(this.scorePanel, BorderLayout.SOUTH);
    }

    private void showMenu() {
        this.dispose();
        SwingUtilities.invokeLater(() -> this.menuWindow.setVisible(true));
        this.menuWindow.showPanel(MenuWindow.MAIN_MENU_CARD);
    }

    @Override
    public void update() {
        synchronized (this.snake) {
            this.scorePanel.setScore(this.snake.getScore());

            if (!this.snake.isAlive() || this.snake.won()) {
                var message = this.snake.won()
                        ? "Mission complete, and how! They're gonna tell stories about this one, Boss!"
                        : "What's wrong? Snake? SNAAAAKE?";
                var title = this.snake.won()
                        ? "Mission complete"
                        : "GAME OVER";
                JOptionPane.showMessageDialog(
                        this, message,
                        title, JOptionPane.PLAIN_MESSAGE, null
                );
                // this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
                this.showMenu();
            }

            var prevTail = this.snake.getPreviousTailPosition();
            if (prevTail != null) {
                this.labelsGrid[prevTail.Y * this.planeSize + prevTail.X].setIcon(
                        this.iconManager.getIcon(IconManager.EMPTY)
                );
            }

            var newHead = this.snake.getHeadPosition();
            this.labelsGrid[newHead.Y * this.planeSize + newHead.X].setIcon(
                    this.iconManager.getIcon(IconManager.SNAKE)
            );

            var applePosition = this.snake.getApplePosition();
            this.labelsGrid[applePosition.Y * this.planeSize + applePosition.X].setIcon(
                    this.iconManager.getIcon(IconManager.APPLE)
            );

            if (!this.useUserInput) {
                this.snake.setDirection(this.snakeAI.getNextDirection(this.snake));
            }
        }
    }

    @Override
    public void run() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }
}
