package snake.ui.gui.window;

import snake.Snake;
import snake.ui.gui.panel.DifficultySelectionPanel;
import snake.ui.gui.panel.MainMenuPanel;
import snake.ui.gui.panel.SettingsPanel;
import snake.util.settings.UI_PANEL_SIZE;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class MenuWindow extends JFrame implements Runnable {
    public static final String MAIN_MENU_CARD = "Main Menu";
    public static final String DIFFICULTY_MENU_CARD = "Difficulty Menu";
    public static final String SETTINGS_CARD = "Settings Menu";

    private final CardLayout cardLayout;

    private final DifficultySelectionPanel difficultySelectionPanel;
    private final SettingsPanel settingsPanel;

    public MenuWindow() {
        super("Metal Gear Solid 3: Snake Eater");

        this.cardLayout = new CardLayout();

        this.difficultySelectionPanel = new DifficultySelectionPanel(this);
        this.settingsPanel = new SettingsPanel(this);

        this.addComponentsToPane(this.getContentPane());

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(this.cardLayout);

        pane.add(new MainMenuPanel(this), MAIN_MENU_CARD);
        pane.add(this.difficultySelectionPanel, DIFFICULTY_MENU_CARD);
        pane.add(this.settingsPanel, SETTINGS_CARD);
    }

    @Override
    public void run() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }

    public void showPanel(String panelName) {
        this.cardLayout.show(this.getContentPane(), panelName);
    }

    public void startGame() {
        var difficulty = this.difficultySelectionPanel.getSelectedDifficulty();
        var planeSize = this.settingsPanel.getSelectedPlaneSize();
        var uiSize = this.settingsPanel.getSelectedUIPanelSize();

        double scale = (double) uiSize.getSize() / (double) planeSize.getSize() / (double) UI_PANEL_SIZE.getIconSize();

        var snake = new Snake(planeSize.getSize(), difficulty.getRate());

        GameWindow gameWindow;

        try {
            gameWindow = new GameWindow(
                    this, snake, scale, difficulty.isNotAuto(), difficulty.getCorrespondingAI()
            );
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        SwingUtilities.invokeLater(() -> this.setVisible(false));
        gameWindow.run();
        snake.run();
    }
}
