package snake.ui.gui.util;

import javax.swing.*;
import java.awt.*;

public final class Utility {
    private static final Color BACKGROUND_COLOUR = Color.BLACK;
    private static final Color FOREGROUND_COLOUR = Color.WHITE;
    private static final Color BORDER_COLOUR = Color.WHITE;

    public static JLabel createLabel(
            String title,
            boolean drawBorder,
            int horizontalAlignment) {
        var label = new JLabel(title);
        label.setBackground(BACKGROUND_COLOUR);
        label.setForeground(FOREGROUND_COLOUR);

        if (drawBorder) {
            label.setBorder(BorderFactory.createLineBorder(BORDER_COLOUR));
        }

        label.setHorizontalAlignment(horizontalAlignment);

        return label;
    }
}
