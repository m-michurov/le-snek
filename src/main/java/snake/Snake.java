package snake;

import snake.util.Updater;

import java.util.ArrayDeque;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public final class Snake implements Runnable {
    public static final int MINIMAL_PLANE_SIZE = 10;
    private static final int INITIAL_SNAKE_SIZE = 3;

    private final int planeSize;
    private final double updateFrequency;
    private Updater updater;

    private DIRECTION nextDirection;
    private DIRECTION currentDirection;

    private final ArrayDeque<Position> snake;
    private Position previousTailPosition;
    private Position applePosition;

    private int score;

    private boolean alive;
    private final Timer timer;

    public Snake(
            int planeSize,
            double updateFrequency)
            throws IllegalArgumentException {
        if (planeSize < MINIMAL_PLANE_SIZE) {
            throw new IllegalArgumentException("invalid plane size");
        }
        this.planeSize = planeSize;
        this.updateFrequency = updateFrequency;

        this.snake = new ArrayDeque<>(INITIAL_SNAKE_SIZE);

        this.score = 0;

        this.nextDirection = DIRECTION.UP;
        this.alive = true;
        this.timer = new Timer();

        this.init();
    }

    public static final class Position {
        public final int X;
        public final int Y;
        private final int planeSize;

        public Position(
                int x,
                int y,
                int planeSize) {
            if (x < 0) {
                this.X = x + ((-x + planeSize - 1) / planeSize) * planeSize;
            } else {
                this.X = x % planeSize;
            }
            if (y < 0) {
                this.Y = y + ((-y + planeSize - 1) / planeSize) * planeSize;
            } else {
                this.Y = y % planeSize;
            }
            this.planeSize = planeSize;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Position && ((Position) obj).X == this.X && ((Position) obj).Y == this.Y;
        }

        @Override
        public int hashCode() {
            // Magic
            int hash = 7;
            hash = 71 * hash + this.X;
            hash = 71 * hash + this.Y;
            return hash;
        }

        @Override
        public String toString() {
            return String.format("(%d, %d)", this.X, this.Y);
        }

        public int distanceTo(Position other) {
            var d1 = Math.abs(other.X - this.X) + Math.abs(other.Y - this.Y);

            var d2 = Math.abs(other.X + this.planeSize - this.X) + Math.abs(other.Y - this.Y);
            var d3 = Math.abs(other.X - this.planeSize - this.X) + Math.abs(other.Y - this.Y);
            var d4 = Math.abs(other.X - this.X) + Math.abs(other.Y + this.planeSize - this.Y);
            var d5 = Math.abs(other.X - this.X) + Math.abs(other.Y - this.planeSize - this.Y);

            var d6 = Math.abs(other.X + this.planeSize - this.X) + Math.abs(other.Y + this.planeSize - this.Y);
            var d7 = Math.abs(other.X - this.planeSize - this.X) + Math.abs(other.Y - this.planeSize - this.Y);
            var d8 = Math.abs(other.X - this.planeSize - this.X) + Math.abs(other.Y + this.planeSize - this.Y);
            var d9 = Math.abs(other.X + this.planeSize - this.X) + Math.abs(other.Y - this.planeSize - this.Y);

            // God forgive me
            return Math.min(d1, Math.min(d2, Math.min(d3, Math.min(d4,
                    Math.min(d5, Math.min(d6, Math.min(d7, Math.min(d8, d9))))))));
        }

        public Position[] getNeighbours() {
            return new Position[] {
                    new Position(this.X - 1, this.Y, this.planeSize),
                    new Position(this.X + 1, this.Y, this.planeSize),
                    new Position(this.X, this.Y - 1, this.planeSize),
                    new Position(this.X, this.Y + 1, this.planeSize)
            };
        }

        public DIRECTION getMinDistanceDirection(Position other) {
            if (this.X != other.X && this.Y != other.Y) {
                throw new IllegalArgumentException(
                        String.format("cannot determine direction towards %s with origin %s", other, this)
                );
            }

            var distance = this.distanceTo(other);

            if (this.X == other.X) {
                if (Math.abs(this.Y - other.Y) > distance) {
                    if (this.Y > other.Y) {
                        return DIRECTION.DOWN;
                    } else {
                        return DIRECTION.UP;
                    }
                } else {
                    if (this.Y > other.Y) {
                        return DIRECTION.UP;
                    } else {
                        return DIRECTION.DOWN;
                    }
                }
            }

            if (Math.abs(this.X - other.X) > distance) {
                if (this.X > other.X) {
                    return DIRECTION.RIGHT;
                } else {
                    return DIRECTION.LEFT;
                }
            } else {
                if (this.X > other.X) {
                    return DIRECTION.LEFT;
                } else {
                    return DIRECTION.RIGHT;
                }
            }
        }
    }

    public enum DIRECTION {
        UP,
        DOWN,
        LEFT,
        RIGHT;

        public boolean isOppositeTo(DIRECTION other) {
            switch (other) {
                case UP:
                    return this == DOWN;
                case DOWN:
                    return this == UP;
                case LEFT:
                    return this == RIGHT;
                case RIGHT:
                    return this == LEFT;
            }

            return false;
        }
    }

    public ArrayDeque<Position> getSnakeSegments() {
        return this.snake;
    }

    public Position getHeadPosition() {
        return this.snake.getFirst();
    }

    public Position getPreviousTailPosition() {
        return this.previousTailPosition;
    }

    public Position getApplePosition() {
        return this.applePosition;
    }

    public void setDirection(DIRECTION newDirection) {
        if (this.currentDirection.isOppositeTo(newDirection)) {
            return;
        }

        this.nextDirection = newDirection;

    }

    public boolean isAlive() {
        return this.alive;
    }

    public int getPlaneSize() {
        return this.planeSize;
    }

    public int getScore() {
        return this.score;
    }

    public void setUpdater(Updater updater) {
        this.updater = updater;
    }

    private void init() {
        this.spawnSnake();
        this.spawnApple();
    }

    public void stop() {
        this.alive = false;
        this.timer.cancel();
    }

    public boolean won() {
        return this.score == this.planeSize * this.planeSize - INITIAL_SNAKE_SIZE;
    }

    private synchronized void move() {
        if (!this.alive) {
            this.stop();
            return;
        }

        this.previousTailPosition = this.snake.removeLast();

        var previousHead = this.snake.getFirst();
        int headX;
        int headY;

        this.currentDirection = this.nextDirection;

        switch (this.currentDirection) {
            case UP:
                headX = previousHead.X;
                headY = previousHead.Y - 1;
                break;
            case DOWN:
                headX = previousHead.X;
                headY = previousHead.Y + 1;
                break;
            case LEFT:
                headX = previousHead.X - 1;
                headY = previousHead.Y;
                break;
            case RIGHT:
                headX = previousHead.X + 1;
                headY = previousHead.Y;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + this.currentDirection);
        }

        var newHead = new Position(headX, headY, this.planeSize);

        if (this.snake.contains(newHead)) {
            this.stop();
        }

        this.snake.addFirst(newHead);

        if (newHead.equals(this.applePosition)) {
            this.score += 1;
            if (this.won()) {
                this.stop();
                return;
            }
            this.spawnApple();
            this.snake.addLast(this.previousTailPosition);
            this.previousTailPosition = null;
        }
    }

    private void spawnSnake() {
        this.snake.clear();

        int centerX = this.planeSize / 2;
        int centerY = this.planeSize / 2;

        this.snake.add(new Position(centerX, centerY - 1, this.planeSize));
        this.snake.add(new Position(centerX, centerY, this.planeSize));
        this.snake.add(new Position(centerX, centerY + 1, this.planeSize));
    }

    private void spawnApple() {
        if (this.won()) {
            return;
        }

        Position newApplePosition;
        do {
            int x = ThreadLocalRandom.current().nextInt(0, planeSize);
            int y = ThreadLocalRandom.current().nextInt(0, planeSize);

            newApplePosition = new Position(x, y, this.planeSize);
        } while (this.snake.contains(newApplePosition));

        this.applePosition = newApplePosition;
    }

    @Override
    public void run() {
        this.alive = true;

        int delay = (int) (1000.0 / this.updateFrequency);

        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Snake.this.move();
                Updater.updateIfNotNull(Snake.this.updater);
            }
        }, delay, delay);
    }
}
